// support ES6 https://kangax.github.io/compat-table/es6/

// XmlHttpRequest => avant
// window.fetch => dans navigateur modernes. Utiliser polyfills si besoin, voir https://github.com/github/fetch

// fetch
// https://fetch.spec.whatwg.org/
// https://github.com/github/fetch

let restApiURL = 'http://localhost:8888/symfony_training/web/app_dev.php/rest';

let filmsResourceURL = restApiURL + '/films';

fetch(filmsResourceURL)
    .then(response => response.json())
    // .then(renderHTMLTableFromFilms)
    // .then(updateMain)
    .then(createVirtualDomFromFilms)
    .then(updateMainFromVirtualDom)
    .catch(console.log);

function createVirtualDomFromFilms(films) {
    let table = document.createElement('table');

    films.forEach(f => {
        let tr = document.createElement('tr');
        tr.addEventListener('click', event => {
            fetch(`${filmsResourceURL}/${f.id}`)
                .then(response => response.json())
                .then(f => {
                    let detail = document.querySelector('.detail');
                    detail.innerHTML =`<h1>${f.title}</h1><p>${f.description}</p>`;
                })
        });
        let tdId = document.createElement('td');
        let textId = document.createTextNode(f.id);
        tdId.appendChild(textId);
        tr.appendChild(tdId);

        let tdTitle = document.createElement('td');
        let textTitle = document.createTextNode(f.title);
        tdTitle.appendChild(textTitle);
        tr.appendChild(tdTitle);

        table.appendChild(tr);
    });

    return table;


}

function updateMainFromVirtualDom(dom) {
    let main = document.querySelector('.main');
    main.appendChild(dom);
}


function renderHTMLTableFromFilms(films) {
    let html = '<table>';
    films.forEach(f => {
       html += renderHTMLTableRowFromFilm(f);
    });
    html += '</table>';

    return html;
}

// rajouter un listener pour l'évènement on click du tr
// comment faire ?
function renderHTMLTableRowFromFilm(film) {
    return `<tr class="film" id="film-${film.id}"><td>${film.id}</td><td>${film.title}</td></tr>`;

}

function updateMain(html) {
    document.querySelector('.main').innerHTML = html;
}

