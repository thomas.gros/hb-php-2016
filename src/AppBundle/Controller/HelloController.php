<?php
/**
 * Created by PhpStorm.
 * User: thomasgros
 * Date: 28/11/16
 * Time: 14:44
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class HelloController extends Controller
{
    /**
     * @Route("/hello")
     */
    public function helloAction()
    {



        return new Response("<h1>Hello World</h1>", 200, ['Content-Type' => 'text/plain']);
//        return new Response("<h1>Hello World</h1>");
    }


    /**
     * @Route("/hello/{name}")
     */
    public function helloNameAction($name) {
        return $this->render('hello/hello-name.html.twig', [ "toto" => $name]);
    }

}