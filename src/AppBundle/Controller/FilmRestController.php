<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Film;
use AppBundle\Entity\Language;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FilmRestController
 * @package AppBundle\Controller
 *
 * @Route("/rest/films")
 */
class FilmRestController extends Controller
{

    /**
     * @Route("", name="rest_film_list")
     * @Method("GET")
     */
    public function listAction(Request $request)
    {
        $films = $this->getDoctrine()
                      ->getRepository('AppBundle:Film')
                      ->findAll();

        return $this->json($films);

//        $jsonFilms = json_encode($films);
//
//        return new Response(
//            $jsonFilms,
//            200,
//            [
//                'Content-Type' => 'application/json'
//            ]
//        );

    }

    /**
     * @Route("", name="rest_film_post")
     * @Method("POST")
     */
    public function postAction(Request $request) {
        $jsonBody = $request->getContent();

        // 2 options pour serializer JSON => OBJECT
        // 1) json_decode
        // 2) le composant serializer symfony (à activer dans config.yml)
        $film = $this
            ->get('serializer')
            ->deserialize($jsonBody, Film::class, 'json');

        $language = $this->getDoctrine()->getManager()->find(Language::class, 1);
        $film->setLanguage($language);

        $this->get('doctrine')
            ->getRepository('AppBundle:Film')
            ->save($film);

        return $this->json([
            'message' => 'success',
            'link' => $this->generateUrl('film_rest_detail', [ 'id' => $film->getId()])
        ]);
    }


    /**
     * @Route("/{id}", name="rest_film_detail")
     */
    public function detailAction($id)
    {

        $film = $this->getDoctrine()
                     ->getRepository('AppBundle:Film')
                     ->find($id);

        if($film === null) {
            // return new Response('{"message":"film not found"}', 404);
            return $this->json(["message" => "film not found"], 404);
        }

        return $this->json($film);

//        $jsonFilm = json_encode($film);
//
//        return new Response(
//            $jsonFilm,
//            200,
//            [
//                'Content-Type' => 'application/json'
//            ]
//        );

    }
}
