<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Film;
use AppBundle\Entity\FilmService;
use AppBundle\Entity\Language;
use AppBundle\Form\FilmType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/films")
 */
class FilmController extends Controller
{

    /**
     * @Route("/list", name="film_list")
     */
    public function listAction(Request $request)
    {
        // $logger = $this->container->get('logger');
        $logger = $this->get('logger');
        $logger->info('message à caractère informatif...');
        $logger->debug('Called FilmController#listAction');

        $nbFilmsPerPage = 10;

        $page = $request->get('page', '1'); // path, sinon get, sinon body
        if (!is_numeric($page)) {
            return $this->redirectToRoute('film_list');
        }

        $page = intval($page); // comment convertir une string en entier

        if ($page < 1) {
            return $this->redirectToRoute('film_list');
        }

        $filmsCount = $this->getDoctrine()
                            ->getRepository('AppBundle:Film')
                            ->count();

        $maxPage = ceil($filmsCount / $nbFilmsPerPage);

        if($page > $maxPage) {
            return $this->redirectToRoute('film_list');
        }

        $offset = ($page - 1) * $nbFilmsPerPage;

        $films = $this->getDoctrine()
                      ->getRepository('AppBundle:Film')
                      ->findBy([], null, $nbFilmsPerPage, $offset);


        // $em->createQuery('SELECT f FROM AppBundle\Entity\Film f');
//        $films = $em->createQuery('SELECT f FROM AppBundle:Film f')// SELECT * FROM film;
//            ->setFirstResult($offset)
//            ->setMaxResults($nbFilmsPerPage)
//            ->getResult();

        $logger->debug('Found films', $films);

        return $this->render('film/list.html.twig', [
            'films' => $films,
            'offset' => $offset,
            'page' => $page,
            'maxPage' => $maxPage
        ]);
    }

    /**
     * @Route("/detail/{id}", name="film_detail")
     */
    public function detailAction($id)
    {
        $film = $this->getDoctrine()
                     ->getRepository('AppBundle:Film')
                     ->find($id);

//        $em = $this->getDoctrine()->getManager();
//        $film = $em->find('AppBundle\Entity\Film', $id);

        $logger = $this->get('logger');
        $logger->info($film);

        dump($film);

        if ($film == null) { // TODO 404
            // return new Response('oups...', 404);
            throw $this->createNotFoundException("Le film demandé n'existe pas");
        }

        return $this->render('film/detail.html.twig', [
            'film' => $film
        ]);
    }


    /**
     * @Route("/new", name="film_new")
     */
    public function createFilm(Request $request)
    {

        $film = new Film(null, '');

        $form = $this->createFormBuilder($film)
            ->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('save', SubmitType::class)
            ->getForm();

        return $this->handleFilmNewEditFormSubmission($form, $request);
    }

    /**
     * @Route("/edit/{id}", name="film_edit")
     */
    public function editFilm(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $film = $em->find(Film::class, $id);

        if ($film == null) {
            throw $this->createNotFoundException("Le film demandé n'existe pas");
        }

        $form = $this->createForm(FilmType::class, $film);

        return $this->handleFilmNewEditFormSubmission($form, $request);
    }

    private function handleFilmNewEditFormSubmission($form, $request)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $film = $form->getData();

            $language = $this->getDoctrine()->getManager()->find(Language::class, 1);
            $film->setLanguage($language);


            $this->getDoctrine()
                ->getRepository('AppBundle:Film')
                ->save($film);

            $this->get('logger')->debug('valid form data', [$film]);

            return $this->redirectToRoute('film_list');
        }

        return $this->render('film/create-film.html.twig', [
            'form' => $form->createView()
        ]);
    }

}
