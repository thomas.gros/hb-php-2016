<?php

namespace AppBundle\Entity;

class FilmService
{

    /**
     * @var Film[]
     */
    private $films;

    /**
     * FilmService constructor.
     */
    public function __construct()
    {
        $this->films = [
            new Film(0, 'Retour vers le futur'),
            new Film(1, 'Avengers'),
            new Film(2, 'Doctor Stranger'),
            new Film(3, 'Wall-e')
        ];
    }

    /**
     * @return array
     */
    public function findAll(): array {
        return $this->films;
    }


    /**
     * @param int $id
     * @return Film or null if no film has the id $id
     */
    public function find(int $id) {
        foreach ($this->films as $film) {
            if($film->getId() === $id) {
                return $film;
            }
        }

        return null;

    }

    public function save(Film $film) {
        // TODO... si id alors UPDATE sinon INSERT
    }


}