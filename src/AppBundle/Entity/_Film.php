<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Film
{

    /**
     * @var int
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @var string
     */
    private $title;

    /**
     * Film constructor.
     * @param int $id
     * @param string $title
     */
    public function __construct($id, $title)
    {
        $this->id = $id;
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title = null)
    {
        $this->title = $title;
    }

    function __toString()
    {
        return "$this->id $this->title";
    }


}